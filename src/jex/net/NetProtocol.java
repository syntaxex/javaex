package jex.net;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Interface de protocole de communication NetServer.
 *
 * @param <Q> Format de requête
 * @param <A> Format de réponse
 */
public interface NetProtocol<Q, A> {
    
    /**
     * Méthode d'envoie des données.<br>
     * Utilisé en principe par la partie client.
     *
     * @param stream
     * @param data
     * @throws IOException
     */
    void push(OutputStream stream, Q data) throws IOException;

    /**
     * Méthode de traitement des données reçues.<br>
     * Utilisé en principe par la partie client.
     *
     * @param stream
     * @return
     * @throws IOException
     */
    A pull(InputStream stream) throws IOException;

    /**
     * Récupération des données envoyées.<br>
     * Utilisé en principe par la partie serveur.
     *
     * @param stream
     * @return
     * @throws IOException
     */
    Q input(InputStream stream) throws IOException;

    /**
     * Envoie de données pour réception.<br>
     * Utilisé en principe par la partie serveur.
     *
     * @param stream
     * @param data
     * @throws IOException
     */
    void output(OutputStream stream, A data) throws IOException;

}
