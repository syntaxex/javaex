package jex.net;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ProtocolException;
import java.nio.charset.Charset;

/**
 * Protocol de base pour la transmission et réception de chaîne texte avec un NetServer.
 */
public class NetBasicStringProtocol implements NetProtocol<String, String> {

    private final NetBasicProtocol protocol;
    private final Charset charset;

    public NetBasicStringProtocol(int maxDataLength) {
        this(Charset.defaultCharset(), maxDataLength);
    }

    public NetBasicStringProtocol(Charset charset, int maxDataLength) {
        this.protocol = new NetBasicProtocol(maxDataLength);
        this.charset = charset;
    }

    /**
     * Encodage de la valeur
     *
     * @param value
     * @return
     */
    private byte[] encode(String value) throws ProtocolException {
        if (value == null) throw new ProtocolException("Data is NULL.");
        return value.getBytes(charset);
    }

    /**
     * Décodage de la valeur
     *
     * @param value
     * @return
     */
    private String decode(byte[] value) throws ProtocolException {
        if (value == null) throw new ProtocolException("Data is NULL.");
        String result = new String(value, charset);
        return result;
    }

    @Override
    public void push(OutputStream stream, String data) throws IOException {
        protocol.push(stream, encode(data));
    }

    @Override
    public String pull(InputStream stream) throws IOException {
        return decode(protocol.pull(stream));
    }

    @Override
    public String input(InputStream stream) throws IOException {
        return decode(protocol.input(stream));
    }

    @Override
    public void output(OutputStream stream, String data) throws IOException {
        protocol.output(stream, encode(data));
    }

}
