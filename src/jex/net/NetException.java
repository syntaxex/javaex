package jex.net;

/**
 * Exception déclenchée par les objets JEXNet.
 */
public class NetException extends Exception {

    public NetException(String message) {
        super(message);
    }

    public NetException(String message, Throwable cause) {
        super(message, cause);
    }

    public NetException(Throwable cause) {
        super(cause);
    }
}
