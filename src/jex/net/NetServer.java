package jex.net;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

/**
 * Classe de serveur réseau.
 *
 * @param <Q> Type des requêtes reçues
 * @param <A> Type des réponses retournées
 */
public class NetServer<Q, A> {

    private static final int DEFAULT_TIMEOUT = 10000;//10s
    private static final int DEFAULT_INPUT_LATENCY = 1;//1ms

    private final int port;
    private final NetInterpreter<Q, A> interpreter;
    private final NetProtocol<Q, A> protocol;
    private final int inputLatency;
    private final List<Thread> connections = new ArrayList<>();
    private ServerSocket serverSocket = null;
    private Thread connectionListener = null;

    public NetServer(int port, NetInterpreter<Q, A> interpreter, NetProtocol<Q, A> protocol) {
        this(port, interpreter, protocol, DEFAULT_INPUT_LATENCY);
    }

    public NetServer(int port, NetInterpreter<Q, A> interpreter, NetProtocol<Q, A> protocol, int inputLatency) {
        this.port = port;
        this.interpreter = interpreter;
        this.protocol = protocol;
        this.inputLatency = inputLatency;
    }

    public NetInterpreter getApi() {
        return interpreter;
    }

    public NetProtocol<Q, A> getProtocol() {
        return protocol;
    }

    /**
     * Instance une nouvelle connexion.
     *
     * @param socket
     * @return
     */
    private Thread newConnection(Socket socket) {
        Thread connection = new Thread(() -> {
            while (!socket.isClosed()) {
                try {
                    //Attente
                    while (socket.getInputStream().available() == 0) {
                        Thread.sleep(inputLatency);
                    }
                    //Réception
                    Q request = protocol.input(socket.getInputStream());
                    if (request == null) continue;
                    //Traitement
                    A reply = interpreter.execute(request);
                    //Réponse
                    if (reply != null) protocol.output(socket.getOutputStream(), reply);
                } catch (IOException ex) {
                    //Erreur
                    try {
                        A error = interpreter.error(ex);
                        if (error != null) protocol.output(socket.getOutputStream(), error);
                    } catch (IOException ignored) {
                    } finally {
                        break;
                    }
                } catch (InterruptedException e) {
                    break;
                }
            }
            if (!socket.isClosed()) {
                try {
                    socket.close();
                } catch (Exception ignored) {
                }
            }
        });
        return connection;
    }

    /**
     * Ouverture de la connexion
     *
     * @throws IOException
     */
    public void start() throws IOException {
        if (isStarted()) return;
        this.serverSocket = new ServerSocket(port); //Ouverture du port d'écoute            
        connectionListener = new Thread(() -> {//Création du processus d'écoute
            while (!serverSocket.isClosed()) {
                try {
                    //Nouvelle demande de connexion
                    Socket socket = serverSocket.accept();
                    socket.setSoTimeout(DEFAULT_TIMEOUT);
                    Thread connection = newConnection(socket);
                    connections.add(connection);
                    connection.start();
                } catch (IOException ex) {
                    //Erreur lors de l'ouverture de la connexion
                    System.err.println("NetServer error : " + ex.getMessage());
                }
            }
        });
        connectionListener.start(); //Attente de nouvelle connexion
    }

    /**
     * Fermeture de la connexion
     *
     * @throws IOException
     */
    public void stop() throws IOException {
        if (serverSocket == null) return;
        serverSocket.close();
        connectionListener = null;
        connections.clear();
    }

    /**
     * Test si le serveur est démarré.
     *
     * @return
     */
    public boolean isStarted() {
        return ((serverSocket != null) && (!serverSocket.isClosed()));
    }

    /**
     * Test si le serveur est arrêté.
     *
     * @return
     */
    public boolean isStopped() {
        return ((serverSocket == null) || (serverSocket.isClosed()));
    }

}
