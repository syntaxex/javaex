package jex.net;

import java.io.IOException;

/**
 * Interface pour les objets de traitement des NetRequest.
 *
 * @param <Q> Requête
 * @param <A> Réponse
 */
public interface NetInterpreter<Q, A> {

    /**
     * Exécute la requête.
     *
     * @param request Requête à exécuter
     * @return Résultat
     */
    A execute(Q request);

    /**
     * Retourne le résultat à retourner en cas d'erreur.
     *
     * @param exception Exception à analyser
     * @return Résultat correspondant
     */
    A error(IOException exception);

}
