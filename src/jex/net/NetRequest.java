package jex.net;

/**
 * Classe de requête pour NetServer.
 *
 * @param <Q> Format de la requête
 * @param <A> Format de la réponse
 */
public class NetRequest<Q, A> {

    public enum State {
        WAITING,
        RUNNING,
        TERMINATE,
        ERROR
    }

    private State state = State.WAITING;
    private Q request;
    private A result = null;
    private NetRequestError error;
    private NetRequestCallback<Q, A> callback;

    public NetRequest(Q request) {
        this(request, null);
    }

    /**
     *
     * @param request
     * @param callback
     */
    public NetRequest(Q request, NetRequestCallback<Q, A> callback) {
        this.request = request;
        this.callback = callback;
    }

    public final Q getRequest() {
        return request;
    }

    public final A getResult() {
        return result;
    }

    public NetRequestError getError() {
        return error;
    }

    /**
     * Mets à jour l'état de la requête.
     *
     * @param result
     * @param error
     * @param state
     */
    protected final void update(A result, NetRequestError error, State state) {
        State oldState = this.state;
        this.result = result;
        this.state = state;
        this.error = error;
        if (oldState == state) return;
        switch (state) {
            case TERMINATE:
            case ERROR:
                if (callback != null) callback.run(this);
                break;
        }
    }

    public State getState() {
        return state;
    }

    public NetRequestCallback<Q, A> getCallback() {
        return callback;
    }

}
