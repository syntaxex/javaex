package jex.net;

import java.io.*;
import java.net.ProtocolException;

/**
 * Protocol de base pour la transmission et réception de données avec un NetServer.
 */
public class NetBasicProtocol implements NetProtocol<byte[], byte[]> {

    private final String currentVersion = "v1.1";
    private final int versionLength = 8;
    private final int sizeLength = 12;
    private int maxDataLength;

    public NetBasicProtocol(int maxDataLength) {
        this.maxDataLength = maxDataLength;
    }

    /**
     * Encode les données.
     *
     * @param data
     * @return
     */
    private byte[] encode(byte[] data) throws ProtocolException {
        if (data == null) throw new ProtocolException("Data is NULL.");
        try {
            //version
            byte[] version = new byte[versionLength];
            byte[] versionBytes = currentVersion.getBytes();
            System.arraycopy(versionBytes, 0, version, 0, versionBytes.length);
            //size
            byte[] size = new byte[sizeLength];
            byte[] sizeBytes = Integer.toString(data.length).getBytes();
            System.arraycopy(sizeBytes, 0, size, 0, sizeBytes.length);
            //content
            int contentSize = versionLength + sizeLength + data.length;
            byte[] content = new byte[contentSize];
            System.arraycopy(version, 0, content, 0, version.length);
            System.arraycopy(size, 0, content, version.length, size.length);
            System.arraycopy(data, 0, content, version.length + size.length, data.length);
            //return
            return content;
        } catch (Exception e) {
            throw new ProtocolException("Unknown protocol encode exception");
        }
    }

    /**
     * Attend la réception d'un segment de données.
     *
     * @param input  Flux d'entrée
     * @param length Nombre d'octets attendus
     * @return Octets lus
     * @throws IOException
     */
    private byte[] read(BufferedInputStream input, int length) throws IOException {
        try {
            final int segmentSize = 1024;
            byte[] segment = new byte[segmentSize];
            byte[] data = new byte[length];
            int unread = length;
            while (unread > 0) {
                int ready = input.available();
                ready = Math.min(ready, unread);
                ready = Math.min(ready, segmentSize);
                int read = input.read(segment, 0, ready);
                if (read == -1) throw new ProtocolException("Abnormal end of buffer in protocol reading");
                System.arraycopy(segment, 0, data, length - unread, read);
                unread -= read;
            }
            return data;
        } catch (IOException e) {
            throw e;
        } catch (Exception e) {
            throw new ProtocolException("Unknown protocol reading exception");
        }
    }

    public int getMaxDataLength() {
        return maxDataLength;
    }

    public void setMaxDataLength(int maxDataLength) {
        this.maxDataLength = maxDataLength;
    }

    @Override
    public void push(OutputStream stream, byte[] data) throws IOException {
        byte[] dataPrepared = encode(data);
        try {
            if ((stream != null) && (dataPrepared != null)) {
                BufferedOutputStream bos = new BufferedOutputStream(stream);
                bos.write(dataPrepared);
                bos.flush();
            }
        } catch (IOException e) {
            stream.close();
            throw e;
        }
    }

    @Override
    public byte[] pull(InputStream stream) throws IOException {
        if (stream == null) return new byte[0];
        BufferedInputStream reader = new BufferedInputStream(stream);
        //version
        byte[] versionBytes = read(reader, versionLength);
        String version = new String(versionBytes).trim();
        if (!currentVersion.equals(version)) throw new ProtocolException("Protocol version not compatible");
        //size
        byte[] size = read(reader, sizeLength);
        int dataLength = 0;
        try {
            dataLength = Integer.parseInt(new String(size).trim());
        } catch (NumberFormatException e) {
            reader.close();
            throw new ProtocolException("Protocol header size info not valid.");
        }
        if (dataLength > maxDataLength) {
            throw new ProtocolException("Max Data Length reached.");
        }
        //Data
        byte[] data = read(reader, dataLength);
        //Result
        return data;
    }

    @Override
    public byte[] input(InputStream stream) throws IOException {
        return pull(stream);
    }

    @Override
    public void output(OutputStream stream, byte[] data) throws IOException {
        push(stream, data);
    }

}
