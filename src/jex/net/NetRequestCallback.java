package jex.net;

/**
 * Interface de retour après exécution d'une requête.
 *
 * @param <Q> Format de requête
 * @param <A> Format de réponse
 */
public interface NetRequestCallback<Q, A> {

    /**
     * Fonction appelée après la fin du traitement d'une requête.
     *
     * @param request Requête
     */
    void run(NetRequest<Q, A> request);

}
