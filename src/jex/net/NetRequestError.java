package jex.net;

/**
 * Exception déclenchée par NetRequest.
 */
public class NetRequestError extends NetException {

    public NetRequestError(String message) {
        super(message);
    }

    public NetRequestError(String message, Throwable cause) {
        super(message, cause);
    }

    public NetRequestError(Throwable cause) {
        super(cause);
    }
    
}
