package jex.net;

import jex.net.NetRequest.State;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.Objects;

/**
 * Classe de client réseau pour NetServer.
 *
 * @param <Q> Format de requête
 * @param <A> Format de réponse
 */
public class NetClient<Q, A> {

    private final String host;
    private final int port;
    private final NetProtocol<Q, A> protocol;
    private Socket soc;

    public NetClient(String host, int port, NetProtocol<Q, A> protocol) {
        this.host = Objects.requireNonNull(host);
        this.port = port;
        this.protocol = Objects.requireNonNull(protocol);
    }

    public String getHost() {
        return host;
    }

    public int getPort() {
        return port;
    }

    public NetProtocol<Q, A> getProtocol() {
        return protocol;
    }

    /**
     * Exécution de la requête
     *
     * @param request
     */
    private void exec(NetRequest<Q, A> request, int timeout) {
        try {
            if (soc == null) return;
            soc.setSoTimeout(timeout);
            request.update(null, null, NetRequest.State.RUNNING);
            push(request);
            A result = pull();
            request.update(result, null, NetRequest.State.TERMINATE);
        } catch (Exception ex) {
            request.update(null, new NetRequestError(ex), State.ERROR);
        }
    }

    /**
     * Envoi de la requête.
     *
     * @param request
     */
    private void push(NetRequest<Q, A> request) throws IOException {
        protocol.push(soc.getOutputStream(), request.getRequest());
    }

    /**
     * Récupération de la réponse du serveur.
     *
     * @return
     */
    private A pull() throws IOException {
        return protocol.pull(soc.getInputStream());
    }

    //PUBLIC    

    /**
     * Ouverture de la connexion.
     *
     * @param timeout
     * @throws java.io.IOException
     */
    public void openConnection(int timeout) throws IOException {
        this.soc = new Socket();
        soc.connect(new InetSocketAddress(host, port), timeout);
    }

    /**
     * Test si la connexion est active.
     *
     * @return
     */
    public boolean isConnected() {
        return (soc != null) && soc.isConnected() && !soc.isClosed();
    }

    /**
     * Déclenchement de la requête, le résultat est stocké dans l'objet request
     *
     * @param request
     * @param timeout
     * @param async
     */
    public void execute(NetRequest<Q, A> request, int timeout, boolean async) {
        Objects.requireNonNull(request);
        if (async) {
            Thread t = new Thread(() -> {
                exec(request, timeout);
            });
            t.start();
        } else {
            exec(request, timeout);
        }
    }

    /**
     * Fermeture de la connexion.
     *
     * @throws java.io.IOException
     */
    public void closeConnection() throws IOException {
        if (soc != null) soc.close();
    }

}
