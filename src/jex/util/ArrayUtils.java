package jex.util;

import java.util.*;

/**
 * Classe utilitaire statique pour les tableaux.
 */
public final class ArrayUtils {

    private ArrayUtils() {
    }

    /**
     * Concaténe dans un tableau.
     *
     * @param <T>  Type du contenu
     * @param vals Valeurs à concaténer
     * @return Tableau
     */
    public static <T> T[] concat(T... vals) {
        return vals;
    }

    /**
     * Concaténe dans un tableau.
     *
     * @param <T>  Type du contenu
     * @param arr1 Tableau 1
     * @param arr2 Tableau 2 ou ensemble de valeurs
     * @return Tableau
     */
    public static <T> T[] concat(T[] arr1, T... arr2) {
        T[] result = Arrays.copyOf(arr1, arr1.length + arr2.length);
        System.arraycopy(arr2, 0, result, arr1.length, arr2.length);
        return result;
    }

    /**
     * Fusionne des tableaux d'octets.
     *
     * @param arr1 Tableau 1
     * @param arr2 Tableau 2
     * @return Tableau final
     */
    public static byte[] concat(byte[] arr1, byte[] arr2) {
        if (arr1 == null) return arr2;
        if (arr2 == null) return arr1;
        int len1 = arr1.length;
        int len2 = arr2.length;
        byte[] res = new byte[len1 + len2];
        System.arraycopy(arr1, 0, res, 0, len1);
        System.arraycopy(arr2, 0, res, len1, len2);
        return res;
    }

    /**
     * Fusionne des tableaux de 'long'.
     *
     * @param arr1 Tableau 1
     * @param arr2 Tableau 2
     * @return Tableau final
     */
    public static long[] concat(long[] arr1, long[] arr2) {
        if (arr1 == null) return arr2;
        if (arr2 == null) return arr1;
        int len1 = arr1.length;
        int len2 = arr2.length;
        long[] res = new long[len1 + len2];
        System.arraycopy(arr1, 0, res, 0, len1);
        System.arraycopy(arr2, 0, res, len1, len2);
        return res;
    }

    /**
     * Convertit une collection de 'Integer' en tableau d'entier primaire.
     *
     * @param arr Collection
     * @return Tableau
     */
    public static int[] asPrimitiveIntArray(Collection<Integer> arr) {
        return arr.stream().mapToInt(v -> v).toArray();
    }

    /**
     * Convertit une collection de 'Long' en tableau de long primaire.
     *
     * @param arr Collection
     * @return Tableau
     */
    public static long[] asPrimitiveLongArray(Collection<Long> arr) {
        return arr.stream().mapToLong(v -> v).toArray();
    }

    /**
     * Convertit une collection de 'Float' en tableau de float primaire.
     *
     * @param arr Collection
     * @return Tableau
     */
    public static float[] asPrimitiveFloatArray(Collection<Float> arr) {
        float[] res = new float[arr.size()];
        int i = 0;
        for (Float value : arr) {
            res[i++] = value;
        }
        return res;
    }

    /**
     * Convertit une collection de 'Double' en tableau de double primaire.
     *
     * @param arr Collection
     * @return Tableau
     */
    public static double[] asPrimitiveDoubleArray(Collection<Double> arr) {
        return arr.stream().mapToDouble(v -> v).toArray();
    }

    /**
     * Transforme un tableau ou une suite de valeurs 'long' en liste.
     *
     * @param arr Tableau ou valeurs
     * @return Liste
     */
    public static List<Long> asList(long[] arr) {
        return Arrays.stream(arr).boxed().toList();
    }

}
