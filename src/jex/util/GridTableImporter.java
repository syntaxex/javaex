package jex.util;

import java.io.IOException;
import java.io.InputStream;

/**
 * Interface pour les composants d'import d'une GridTable.
 */
public interface GridTableImporter<V> extends KeyTableImporter<GridRowKey<V>, GridColumnKey<V>, V> {

    /**
     * Importe vers une GridTable.
     *
     * @param input   Flux d'entrée
     * @throws IOException Exception levée lors de la lecture
     */
    GridTable<V> input(InputStream input) throws IOException;

}
