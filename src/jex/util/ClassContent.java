package jex.util;

import java.util.Map;
import java.util.Objects;

public class ClassContent<T> {

    private final ClassAccessor<T> accessor;
    private final Map<String, String> values;

    ClassContent(ClassAccessor<T> accessor, Map<String, String> values) {
        this.accessor = Objects.requireNonNull(accessor);
        this.values = Objects.requireNonNull(values);
    }

    public ClassAccessor<T> getAccessor() {
        return accessor;
    }

    public String getValue(String propertyName, String defaultValue) {
        return values.getOrDefault(propertyName, defaultValue);
    }

}
