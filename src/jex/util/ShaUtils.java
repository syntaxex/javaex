package jex.util;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;

/**
 * Classe utilitaire pour SHA-512.
 */
public final class ShaUtils {

    /**
     * Convertit une chaine en SHA-512.
     *
     * @param str Chaine à traiter
     * @return Résultat
     */
    public static byte[] toSha512(String str) {
        return toSha512(str, null);
    }

    /**
     * Convertit une chaine en SHA-512.
     *
     * @param str  Chaine à traiter
     * @param salt Chaine de salaison
     * @return Résultat
     */
    public static byte[] toSha512(String str, String salt) {
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-512");
            if ((salt != null) && !salt.isEmpty()) md.update(salt.getBytes(StandardCharsets.UTF_8));
            byte[] bytes = md.digest(str.getBytes(StandardCharsets.UTF_8));
            return bytes;
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    /**
     * Hash la chaine en SHA-512
     *
     * @param str Chaine à traiter
     * @return Résultat
     */
    public static String toStringSha512(String str) {
        return toStringSha512(str, null);
    }

    /**
     * Hash la chaine en SHA-512.
     *
     * @param str  Chaine à traiter
     * @param salt Chaine de salaison
     * @return Résultat
     */
    public static String toStringSha512(String str, String salt) {
        return ByteUtils.toHexaString(toSha512(str, salt));
    }

    private ShaUtils() {
    }

}
