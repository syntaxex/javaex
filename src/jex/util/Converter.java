package jex.util;

/**
 * Interface de conversion de valeur.
 *
 * @param <T> Type de valeur en entrée
 * @param <R> Type de valeur en sortie
 */
public interface Converter<T, R> {
    /**
     * Converti une valeur.
     *
     * @param val Valeur
     * @return Valeur convertie
     */
    R convert(T val);
}
