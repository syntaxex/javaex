package jex.util;

import java.util.Comparator;

/**
 * Comparateur de chaine.
 */
public class StringComparator implements Comparator<String> {

    private final boolean ignoreCase;
    private final ComparatorPosition blankPosition;

    /**
     * Constructeur d'un comparateur de chaîne texte.
     */
    public StringComparator(boolean ignoreCase, ComparatorPosition blankPosition) {
        this.ignoreCase = ignoreCase;
        this.blankPosition = (blankPosition != null) ? blankPosition : ComparatorPosition.START;
    }

    @Override
    public int compare(String s1, String s2) {
        if ((s1 == null) && (s2 == null)) return 0;
        if (s1 == null) return (blankPosition == ComparatorPosition.END) ? 1 : -1;
        if (s2 == null) return (blankPosition == ComparatorPosition.END) ? -1 : 1;
        if (s1.isBlank() && s2.isBlank()) return s1.compareTo(s2);
        if (s1.isBlank()) return (blankPosition == ComparatorPosition.END) ? 1 : -1;
        if (s2.isBlank()) return (blankPosition == ComparatorPosition.END) ? -1 : 1;
        return (ignoreCase) ? s1.compareToIgnoreCase(s2) : s1.compareTo(s2);
    }

}
