package jex.util;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;

/**
 * Classe pour gérer une table de données sous forme de chaînes.
 */
public class StringGridTable extends GridTable<String> {

    /**
     * Crée une table de donnée depuis un objet Map.<br>
     * Chaque ligne représente une entrée.
     * La colonne 1 correspond à la clé sous sa forme texte "toString()".<br>
     * La colonne 2 correspond à la valeur sous sa forme texte "toString()".
     *
     * @param map Map
     * @return StringGridTable contenant la représentation texte des objets de la Map
     */
    public static StringGridTable fromMap(Map<?, ?> map) {
        StringGridTable table = new StringGridTable();
        int index = 0;
        for (Map.Entry<?, ?> entry : map.entrySet()) {
            Object key = entry.getKey();
            Object value = entry.getValue();
            table.setValue(index, 0, (key != null) ? key.toString() : null);
            table.setValue(index, 1, (value != null) ? value.toString() : null);
            index++;
        }
        return table;
    }

    /**
     * Crée une table de donnée depuis un import CSV.
     *
     * @param input     Données du CSV
     * @param separator Caractère de séparation des valeurs
     * @param charset   Charset d'origine
     * @return StringGridTable
     * @throws IOException Exception levée lors de la lecture
     */
    public static StringGridTable fromCSV(InputStream input, Charset charset, char separator) throws IOException {
        Objects.requireNonNull(input);
        Objects.requireNonNull(charset);
        CSVImporter importer = new CSVImporter(charset, separator);
        return importer.getStringGridTableImporter().input(input);
    }

    /**
     * Crée une table de donnée depuis un import CSV.
     *
     * @param input     Données du CSV
     * @param separator Caractère de séparation des valeurs
     * @param charset   Charset d'origine
     * @return StringGridTable
     * @throws IOException Exception levée lors de la lecture
     */
    public static StringGridTable fromCSV(byte[] input, Charset charset, char separator) throws IOException {
        return fromCSV(new ByteArrayInputStream(input), charset, separator);
    }

    /**
     * Crée une table de donnée depuis une chaine texte CSV.
     *
     * @param input     Données du CSV
     * @param separator Caractère de séparation des valeurs
     * @return StringGridTable
     * @throws IOException Exception levée lors de la lecture
     */
    public static StringGridTable fromStringCSV(String input, char separator) throws IOException {
        return fromCSV(new ByteArrayInputStream(input.getBytes(StandardCharsets.UTF_8)), StandardCharsets.UTF_8, separator);
    }

    /**
     * Exporte les données.
     *
     * @param data      Source de données
     * @param output    Flux de sorti
     * @param separator Caractère de séparation des valeurs
     * @param charset   Charset
     * @throws IOException Exception levée lors de l'écriture
     */
    public static void toCSV(StringGridTable data, OutputStream output, Charset charset, char separator) throws IOException {
        Objects.requireNonNull(data);
        Objects.requireNonNull(output);
        Objects.requireNonNull(charset);
        CSVExporter exporter = new CSVExporter(charset, separator);
        data.export(exporter.getGridTableExporter(), output);
    }

    /**
     * Exporte les données en chaîne texte CSV.
     *
     * @param data      Source de données
     * @param separator Caractère de séparation des valeurs
     * @return Chaîne texte CSV
     */
    public static String toStringCSV(StringGridTable data, char separator) {
        StringBuilder csv = new StringBuilder();
        for (int rowIndex = 0; rowIndex < data.countRows(); rowIndex++) {
            for (int colIndex = 0; colIndex < data.countColumns(); colIndex++) {
                if (colIndex != 0) csv.append(separator);
                String value = data.getValue(rowIndex, colIndex, null);
                String outputValue = (value != null) ? CSVUtils.toCSVValue(value, separator) : "";
                csv.append(outputValue);
            }
            csv.append("\r\n");
        }
        return csv.toString();
    }

    /**
     * Trim toutes les valeurs du tableau.
     */
    public void trimAllValues() {
        List<GridRowKey<String>> rows = getRows();
        List<GridColumnKey<String>> cols = getColumns();
        for (GridRowKey<String> row : rows) {
            for (GridColumnKey<String> col : cols) {
                if (!existValue(row, col)) continue;
                String value = getValue(row, col, null);
                if (value == null) continue;
                String trim = value.trim();
                if (!value.equals(trim)) setValue(row, col, trim);
            }
        }
    }

    /**
     * Exporte les données.
     *
     * @param output    Flux de sorti
     * @param separator Caractère de séparation des valeurs
     * @param charset   Charset
     * @throws IOException Exception levée lors de l'écriture
     */
    public void toCSV(OutputStream output, Charset charset, char separator) throws IOException {
        StringGridTable.toCSV(this, output, charset, separator);
    }

    /**
     * Essaye de convertir 2 colonnes de la table en un objet Map.
     *
     * @param indexKeyColumn   Index de la colonne contenant les clés
     * @param indexValueColumn Index de la colonne contenant les valeurs
     * @param <K>              Type de la clé
     * @param <V>              Type de la valeur
     * @return Map ou Null en cas d'échec
     */
    public <K, V> Map<K, V> toMap(int indexKeyColumn, Function<String, K> keyParser, int indexValueColumn, Function<String, V> valueParser) {
        Objects.requireNonNull(keyParser);
        Objects.requireNonNull(valueParser);
        try {
            Map<K, V> map = new HashMap<>();
            for (int rowIndex = 0; rowIndex < countRows(); rowIndex++) {
                String key = getValue(rowIndex, indexKeyColumn, null);
                String value = getValue(rowIndex, indexValueColumn, null);
                map.put(keyParser.apply(key), valueParser.apply(value));
            }
            return map;
        } catch (Exception ignored) {
            return null;
        }
    }

}
