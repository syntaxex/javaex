package jex.util;

import java.math.BigDecimal;
import java.util.Comparator;

/**
 * Comparateur de chaines de type numérique.
 */
public class NumberStringComparator implements Comparator<String> {

    private final ComparatorPosition blankPosition;

    public NumberStringComparator(ComparatorPosition blankPosition) {
        this.blankPosition = blankPosition;
    }

    @Override
    public int compare(String s1, String s2) {
        BigDecimal n1;
        try {
            n1 = new BigDecimal(s1);
        } catch (Exception e) {
            n1 = null;
        }
        BigDecimal n2;
        try {
            n2 = new BigDecimal(s2);
        } catch (Exception e) {
            n2 = null;
        }
        if ((n1 == null) && (n2 == null)) return 0;
        if (n1 == null) return (blankPosition == ComparatorPosition.END) ? 1 : -1;
        if (n2 == null) return (blankPosition == ComparatorPosition.END) ? -1 : 1;
        return n1.compareTo(n2);
    }

}
