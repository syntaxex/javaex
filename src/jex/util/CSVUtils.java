package jex.util;

import java.util.Objects;

/**
 * Classe utilitaire pour l'utilisation des CSV.
 */
public class CSVUtils {

    private CSVUtils() {
    }

    /**
     * Converti une chaine texte en valeur CSV.
     *
     * @param value           Chaine texte à convertir
     * @param valuesSeparator Caractère utilisé pour la séparation des valeurs CSV (utile pour l'échappement)
     * @return Valeur CSV
     */
    public static String toCSVValue(String value, char valuesSeparator) {
        return toCSVValue(value, valuesSeparator, false);
    }

    /**
     * Converti une chaine texte en valeur CSV.
     *
     * @param value           Chaine texte à convertir
     * @param valuesSeparator Caractère utilisé pour la séparation des valeurs CSV (utile pour l'échappement)
     * @param forceQuotes     Force la mise entre guillemets même si ce n'est pas nécessaire
     * @return Valeur CSV
     */
    public static String toCSVValue(String value, char valuesSeparator, boolean forceQuotes) {
        if (value == null) return "";
        boolean escaped = false;
        //detect quotes requested
        if (!forceQuotes) {
            final int len = value.length();
            for (int i = 0; i < len; i++) {
                char c = value.charAt(i);
                if ((c == '"') || (c == '\r') || (c == '\n') || (c == valuesSeparator)) {
                    escaped = true;
                    break;
                }
            }
        }
        //format (if necessary) and return CSV Value
        return (forceQuotes || escaped)
                ? "\"" + value.replace("\"", "\"\"") + "\""
                : value;
    }

    /**
     * Converti une chaine texte en valeur CSV.
     *
     * @param csvValue Valeur CSV
     * @param trim     Trim la valeur
     * @return Chaine texte
     */
    public static String fromCSVValue(String csvValue, boolean trim) {
        Objects.requireNonNull(csvValue);
        String trimValue = csvValue.trim();
        int trimValueLen = trimValue.length();
        String value;
        //detect quotes
        if ((trimValueLen >= 2) && (trimValue.startsWith("\"")) && (trimValue.endsWith("\""))) {
            value = trimValue.substring(0, trimValueLen - 1);
            value = value.replace("\"\"", "\"");
        } else {
            value = csvValue;
        }
        //return
        return trim ? value.trim() : value;
    }

    /**
     * Essaye de détecter le séparateur de champs.
     *
     * @param content   Contenu CSV
     * @param separator Séparateur à tester
     * @return
     */
    public static boolean checkSeparator(byte[] content, char separator) {
        int target = 0;
        int count = 0;
        boolean firstLine = true;
        boolean inString = false;
        char lastChar = '\0';
        for (byte b : content) {
            char c = (char) b;
            if (c == '"') inString = !inString; //String escaper
            if (!inString) { //Ignore String content
                if ((c == '\r') || (c == '\n')) { //end of line
                    if ((lastChar != '\r') && (lastChar != '\n')) {
                        if (count == 0) {
                            return false; //no separator founded
                        }
                        if (count != target) {
                            return false;//checked error
                        }
                        count = 0;
                        firstLine = false;
                    }
                } else {
                    if (c == separator) count++; //separator founded
                    if (firstLine) target = count; //update target if first line
                    if (count > target) {
                        return false; //too separators founded
                    }
                }
            }
            lastChar = c;
        }
        return true;
    }

    /**
     * Essaye de détecter le séparateur de champs.
     *
     * @param content Contenu CSV
     * @return Caractère séparateur ou NULL si non trouvé
     */
    public static Character detectSeparator(byte[] content) {
        if (checkSeparator(content, '\t')) return '\t';
        if (checkSeparator(content, '|')) return '|';
        if (checkSeparator(content, ';')) return ';';
        if (checkSeparator(content, ',')) return ',';
        return null;
    }
}
