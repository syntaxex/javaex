package jex.util;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.List;
import java.util.Map;

/**
 * Classe utilitaire de gestion des dates.
 */
public final class DateTimeUtils {

    public static final DateTimeFormatter COMPACT_LOCAL_DATETIME = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
    public static final DateTimeFormatter COMPACT_LOCAL_DATE = DateTimeFormatter.ofPattern("yyyyMMdd");
    public static final DateTimeFormatter STANDARD_LOCAL_DATETIME = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
    public static final DateTimeFormatter HYBRID_LOCAL_DATETIME = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
    public static final DateTimeFormatter STANDARD_LOCAL_DATE = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    public static final DateTimeFormatter HYBRID_LOCAL_DATE = DateTimeFormatter.ofPattern("yyyy/MM/dd");

    private DateTimeUtils() {
    }

    /**
     * Essaye de détecter le format.
     *
     * @param date  Date ou DateHeure sous forme de chaîne
     * @param regex Tableau de correspondance (Regex à tester = DateTimeFormatter correspondant)
     * @return Format détecté ou NULL si non trouvé
     */
    public static DateTimeFormatter tryDetectFormat(String date, Map<String, DateTimeFormatter> regex) {
        if ((date == null) || date.isEmpty()) return null;
        List<String> validRegexList = regex.keySet().stream().filter(date::matches).toList();
        return (validRegexList.size() == 1)
                ? regex.get(validRegexList.getFirst())
                : null;
    }

    /**
     * Essaye de parser une chaine date locale.
     *
     * @param value       Chaîne date
     * @param formatter   Formateur
     * @param defaultDate Valeur par défaut
     * @return Résultat
     */
    public static LocalDate tryParseDate(String value, DateTimeFormatter formatter, LocalDate defaultDate) {
        if (value == null) return defaultDate;
        if (formatter == null) return defaultDate;
        try {
            return LocalDate.parse(value, formatter);
        } catch (DateTimeParseException e) {
            return defaultDate;
        }
    }

    /**
     * Essaye de parser une chaine date-heure locale.
     *
     * @param value       Chaîne date-heure
     * @param formatter   Formateur
     * @param defaultDate Valeur par défaut
     * @return Résultat
     */
    public static LocalDateTime tryParseDateTime(String value, DateTimeFormatter formatter, LocalDateTime defaultDate) {
        if (value == null) return defaultDate;
        if (formatter == null) return defaultDate;
        try {
            return LocalDateTime.parse(value, formatter);
        } catch (DateTimeParseException e) {
            return defaultDate;
        }
    }

}
