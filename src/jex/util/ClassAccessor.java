package jex.util;

import java.io.FileInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.*;

public class ClassAccessor<T> {

    private final LinkedHashMap<String, ClassProperty<T>> properties = new LinkedHashMap<>();

    public ClassAccessor() {
    }

    public List<ClassContent<T>> parseCSV(InputStream input, Charset charset, char separator, String defaultValue) {
        try {
            StringGridTable table = StringGridTable.fromCSV(input, charset, separator);
            if (table == null) return null;
            int nbRows = table.countRows();
            if (nbRows < 2) return null;
            int nbCols = table.countColumns();
            if (nbCols < 1) return null;
            List<String> heads = table.getRowValues(0, defaultValue);
            List<ClassContent<T>> contents = new ArrayList<>();
            for (int r = 1; r < nbRows; r++) {
                Map<String, String> content = new HashMap<>();
                for (int c = 0; c < nbCols; c++) {
                    content.put(heads.get(c), table.getValue(r, c, defaultValue));
                }
                contents.add(new ClassContent<>(this, content));
            }
            return contents;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public void toCSV(List<T> objects, OutputStream output, Charset charset, char separator) {
        try {
            StringGridTable table = new StringGridTable();
            List<ClassProperty<T>> props = getProperties();
            int c = 0;
            for (ClassProperty<T> prop : props) {
                int r = 1;
                table.setValue(0, c, prop.getName());
                for (T obj : objects) table.setValue(r++, c, prop.getValue(obj));
                c++;
            }
            table.toCSV(output, charset, separator);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public void addProperty(ClassProperty<T> property) {
        this.properties.put(property.getName(), property);
    }

    public List<String> getPropertyNames() {
        return properties.keySet().stream().toList();
    }

    public List<ClassProperty<T>> getProperties() {
        return properties.values().stream().toList();
    }

    public ClassProperty<T> getProperty(String key) {
        return properties.get(key);
    }
}
