package jex.util;

import java.lang.ref.WeakReference;

public class GridColumnKey<V> {

    private final WeakReference<GridTable<V>> tableReference;

    GridColumnKey(GridTable<V> table) {
        this.tableReference = new WeakReference<>(table);
    }

    @Override
    public String toString() {
        GridTable<V> table = tableReference.get();
        return String.valueOf((table != null) ? table.getColumns().indexOf(this) + 1 : 0);
    }

}
