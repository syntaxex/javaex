package jex.util;

/**
 * Classe utilitaire pour la gestion des octets.
 */
public final class ByteUtils {

    private ByteUtils() {
    }

    /**
     * Convertit un tableau d'octets en chaîne hexadécimale.
     *
     * @param arr Tableau d'octets
     * @return Chaine hexadécimale
     */
    public static String toHexaString(byte[] arr) {
        if ((arr == null) || (arr.length == 0)) return "";
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < arr.length; i++) {
            sb.append(String.format("%02x", arr[i] & 0xff));
        }
        return sb.toString();
    }

}
