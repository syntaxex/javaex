package jex.util;

import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

/**
 * Interface pour les données gérées sous formes de tables.
 */
public interface Table<R, C, V> {

    /**
     * Crée une ligne.
     *
     * @param key Clé de la ligne
     * @return Retourne vrai si ajouté ou Faux si déjà présente
     * @throws IOException Erreur d'accès aux données
     */
    boolean createRow(R key) throws IOException;

    /**
     * Test si une ligne existe.
     *
     * @param key Clé de la ligne
     * @return Retourne vrai si présent
     * @throws IOException Erreur d'accès aux données
     */
    boolean existRow(R key) throws IOException;

    /**
     * Test si une ligne contient des valeurs.
     *
     * @param key Clé de la ligne
     * @return Retourne vrai si des valeurs sont présentes
     * @throws IOException Erreur d'accès aux données
     */
    boolean existRowValues(R key) throws IOException;

    /**
     * Vide une ligne.
     *
     * @return Clé de ligne
     * @throws IOException Erreur d'accès aux données
     */
    void clearRow(R key) throws IOException;

    /**
     * Supprime une ligne.
     *
     * @return Clé de ligne
     * @throws IOException Erreur d'accès aux données
     */
    void deleteRow(R key) throws IOException;

    /**
     * Retourne les lignes.
     *
     * @return Clés des lignes
     * @throws IOException Erreur d'accès aux données
     */
    List<R> getRows() throws IOException;

    /**
     * Retourne le nombre de lignes.
     *
     * @return Nombre
     * @throws IOException Erreur d'accès aux données
     */
    int countRows() throws IOException;

    /**
     * Retourne les valeurs d'une ligne.
     *
     * @param row          Clé de ligne
     * @param defaultValue Valeur par défaut
     * @return Valeurs
     * @throws IOException Erreur d'accès aux données
     */
    List<V> getRowValues(R row, V defaultValue) throws IOException;

    /**
     * Crée une nouvelle colonne.
     *
     * @param key Clé de la colonne
     * @return Retourne vrai si ajoutée ou Faux si déjà présente
     * @throws IOException Erreur d'accès aux données
     */
    boolean createColumn(C key) throws IOException;

    /**
     * Test si la colonne existe.
     *
     * @param key Clé de la colonne
     * @return Retourne vrai si la colonne existe
     * @throws IOException Erreur d'accès aux données
     */
    boolean existColumn(C key) throws IOException;

    /**
     * Test si la colonne contient des valeurs.
     *
     * @param key Clé de la colonne
     * @return Retourne vrai si des valeurs sont présentes
     * @throws IOException Erreur d'accès aux données
     */
    boolean existColumnValues(C key) throws IOException;

    /**
     * Vide une colonne.
     *
     * @return Clé de colonne
     * @throws IOException Erreur d'accès aux données
     */
    void clearColumn(C key) throws IOException;

    /**
     * Supprime une colonne.
     *
     * @return Clé de colonne
     * @throws IOException Erreur d'accès aux données
     */
    void deleteColumn(C key) throws IOException;

    /**
     * Retourne les colonnes.
     *
     * @return Clés des colonnes
     * @throws IOException Erreur d'accès aux données
     */
    List<C> getColumns() throws IOException;

    /**
     * Retourne le nombre de colonnes.
     *
     * @return Nombre
     * @throws IOException Erreur d'accès aux données
     */
    int countColumns() throws IOException;

    /**
     * Retourne les valeurs d'une colonne.
     *
     * @param col          Clé de colonne
     * @param defaultValue Valeur par défaut
     * @return Valeurs
     * @throws IOException Erreur d'accès aux données
     */
    List<V> getColumnValues(C col, V defaultValue) throws IOException;

    /**
     * Retourne une valeur.
     *
     * @param rowKey       Clé de la ligne
     * @param colKey       Clé de la colonne
     * @param defaultValue Valeur par défaut si la ligne ou la colonne n'est pas trouvée, ou si la valeur est vide
     * @return Valeur
     * @throws IOException Erreur d'accès aux données
     */
    V getValue(R rowKey, C colKey, V defaultValue) throws IOException;

    /**
     * Demande la valeur correspondant à la ligne et la colonne.
     *
     * @param rowKey       Clé de la ligne
     * @param colKey       Clé de la colonne
     * @param defaultValue Valeur par défaut si la valeur est vide
     * @return Valeur
     * @throws IOException          Erreur d'accès aux données
     * @throws KeyNotFoundException Exception levée si clé de ligne ou de colonne n'est pas trouvée
     */
    V requestValue(R rowKey, C colKey, V defaultValue) throws IOException, KeyNotFoundException;

    /**
     * Ajoute ou modifie la valeur. Les lignes et colonnes non présentes sont créées.
     *
     * @param rowKey Clé de la ligne
     * @param colKey Clé de la colonne
     * @param value  Valeur
     * @throws IOException Erreur d'accès aux données
     */
    void setValue(R rowKey, C colKey, V value) throws IOException;

    /**
     * Modifie la valeur correspondant à la ligne et la colonne.
     *
     * @param rowKey Clé de la ligne
     * @param colKey Clé de la colonne
     * @param value  Valeur
     * @throws IOException          Erreur d'accès aux données
     * @throws KeyNotFoundException Exception levée lorsqu'une clé de ligne ou de colonne n'est pas trouvée
     */
    void updateValue(R rowKey, C colKey, V value) throws IOException, KeyNotFoundException;

    /**
     * Indique si une valeur existe.
     *
     * @param rowKey Clé de la ligne
     * @param colKey Clé de la colonne
     * @return Une valeur existe
     * @throws IOException Erreur d'accès aux données
     */
    boolean existValue(R rowKey, C colKey) throws IOException;

    /**
     * Supprime une valeur.
     *
     * @param rowKey Clé de la ligne
     * @param colKey Clé de la colonne
     * @throws IOException Erreur d'accès aux données
     */
    void deleteValue(R rowKey, C colKey) throws IOException;

    /**
     * Supprime toutes les valeurs, lignes et colonnes de la table.
     *
     * @throws IOException Erreur d'accès aux données
     */
    void deleteAll() throws IOException;

    /**
     * Converti les clés de lignes.
     *
     * @param rowKeyConverter    Convertisseur de clé de ligne
     * @param columnKeyConverter Convertisseur de clé de colonne
     * @param valueConverter     Convertisseur de valeur
     * @param <N>                Nouveau type de clé de ligne
     * @param <M>                Nouveau type de clé de colonne
     * @param <W>                Nouveau type de valeur
     * @return Table convertie
     */
    <N, M, W> Table<N, M, W> convert(Converter<R, N> rowKeyConverter, Converter<C, M> columnKeyConverter, Converter<V, W> valueConverter);

    /**
     * Exporte les données.
     *
     * @param exporter Outil d'export des données
     * @param output   Flux de sorti
     * @throws IOException Exception levée lors de l'écriture
     */
    void export(KeyTableExporter<R, C, V> exporter, OutputStream output) throws IOException;

}
