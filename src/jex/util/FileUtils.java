package jex.util;

import java.text.DecimalFormat;

/**
 * Classe statique de manipulation des fichiers.
 */
public final class FileUtils {

    private FileUtils() {
    }

    /**
     * Retourne une chaîne texte représentant la taille du fichier.
     *
     * @param size    Taille
     * @param decimal Nombre de décimales
     * @return Chaîne texte
     */
    public static String convertSizeToStringSize(long size, int decimal) {
        return convertSizeToStringSize(size, decimal, false);
    }

    /**
     * Retourne une chaîne texte représentant la taille du fichier.
     *
     * @param size       Taille
     * @param decimal    Nombre de décimales
     * @param decimalFix Nombre de décimales est fixe (sinon décimales significatives uniquement)
     * @return Chaîne texte
     */
    public static String convertSizeToStringSize(long size, int decimal, boolean decimalFix) {
        double factor = Math.pow(10, decimal);
        int sector = 0;
        String[] unit = new String[]{"B", "KB", "MB", "GB", "TB", "PB"};
        double trunc = size;
        while ((sector < 6) && (1024 <= trunc)) {
            trunc = trunc / 1024;
            sector++;
        }
        double value = Math.round(trunc * factor) / factor;
        DecimalFormat df = new DecimalFormat("#" + ((decimal > 0) ? "." + (decimalFix ? "0" : "#").repeat(decimal) : ""));
        return df.format(value) + " " + unit[sector];
    }

    /**
     * Retourne le nom d'un fichier.
     *
     * @param filename      Nom du fichier.
     * @param withExtension Indique si l'extension doit être conservée.
     * @return Nom du fichier
     */
    public static String extractName(String filename, boolean withExtension) {
        if (filename == null) return "";
        return (withExtension) ? filename.replaceAll(".*[\\\\/]", "") : filename.replaceAll(".*[\\\\/]|\\.[^\\.]*$", "");
    }

    /**
     * Retourne l'extension d'un fichier.
     *
     * @param filename Nom du fichier.
     * @param withDot  Indique si le point doit être ajouté.
     * @return Extension du fichier
     */
    public static String extractExtension(String filename, boolean withDot) {
        if (filename == null) return "";
        String ext = filename.replaceAll(".*[\\\\/]|[^\\.]*\\.", "");
        return (withDot && !ext.isEmpty()) ? "." + ext : ext;
    }

}
