package jex.util;

import java.math.BigDecimal;
import java.util.Comparator;
import java.util.List;

/**
 * Comparateur de chaine dont les accents, casses, et nombres sont triés de manière "naturel".
 */
public class NaturalStringComparator implements Comparator<String> {

    private final boolean ignoreCase;
    private final ComparatorPosition blankPosition;

    public NaturalStringComparator(boolean ignoreCase, ComparatorPosition blankPosition) {
        this.ignoreCase = ignoreCase;
        this.blankPosition = blankPosition;
    }

    private int wordCompare(String word1, String word2) {
        List<String> mix1 = StringUtils.splitNumerics(word1, true);
        List<String> mix2 = StringUtils.splitNumerics(word2, true);
        int mixSize1 = mix1.size();
        int mixSize2 = mix2.size();
        for (int i = 0; i < Math.min(mixSize1, mixSize2); i++) {
            String val1 = mix1.get(i);
            String val2 = mix2.get(i);
            try {
                BigDecimal num1 = new BigDecimal(val1);
                BigDecimal num2 = new BigDecimal(val2);
                int cmp = num1.compareTo(num2);
                if (cmp != 0) return cmp;
            } catch (NumberFormatException e) {
                int cmp = (ignoreCase) ? val1.compareToIgnoreCase(val2) : val1.compareTo(val2);
                if (cmp != 0) return cmp;
            }
        }
        return Integer.compare(mixSize1, mixSize2);
    }

    @Override
    public int compare(String s1, String s2) {
        List<String> list1 = StringUtils.splitWords(s1, true);
        List<String> list2 = StringUtils.splitWords(s2, true);
        if (list1.isEmpty() && list2.isEmpty()) return 0;
        if (list1.isEmpty()) return (blankPosition == ComparatorPosition.END) ? 1 : -1;
        if (list2.isEmpty()) return (blankPosition == ComparatorPosition.END) ? -1 : 1;
        int listSize1 = list1.size();
        int listSize2 = list2.size();
        for (int i = 0; i < Math.min(listSize1, listSize2); i++) {
            String word1 = list1.get(i);
            String word2 = list2.get(i);
            int cmp = wordCompare(word1, word2);
            if (cmp != 0) return cmp;
        }
        return Integer.compare(listSize1, listSize2);
    }
}
