package jex.util;

import java.util.Objects;

/**
 * Classe pour gérer une clé à 2 entrées.
 */
public class DoubleKey<K, L> {

    private final K key1;
    private final L key2;

    public DoubleKey(K key1, L key2) {
        this.key1 = key1;
        this.key2 = key2;
    }

    public K getKey1() {
        return key1;
    }

    public L getKey2() {
        return key2;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DoubleKey valueReference)) return false;
        return Objects.equals(key1, valueReference.key1) && Objects.equals(key2, valueReference.key2);
    }

    @Override
    public int hashCode() {
        return Objects.hash(key1, key2);
    }

}
