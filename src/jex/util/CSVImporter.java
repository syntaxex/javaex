package jex.util;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Objects;

public class CSVImporter {

    public static final Converter<String, String> DEFAULT_CONVERTOR = val -> val;

    private final Charset charset;
    private final char separator;

    public CSVImporter(Charset charset, char separator) {
        this.charset = Objects.requireNonNull(charset);
        this.separator = separator;
    }

    /**
     * Converti un ensemble d'octet en chaîne texte.
     *
     * @param value   Octets à convertir
     * @param charset Charset d'origine
     * @return Chaîne texte
     */
    private static String toString(byte[] value, Charset charset) {
        return ((value != null) && (value.length > 0)) ? new String(value, charset) : "";
    }

    public Charset getCharset() {
        return charset;
    }

    public char getSeparator() {
        return separator;
    }

    /**
     * Retourne un importeur de données dans une StringGridTable.
     *
     * @return Importeur
     */
    public StringGridTableImporter getStringGridTableImporter() {
        return (InputStream input) -> {
            StringGridTable data = new StringGridTable();
            int row = 0;
            int col = 0;
            long pos = -1;
            ByteArrayOutputStream value = new ByteArrayOutputStream();
            boolean string = false;
            int lastByte = -1;
            int inputByte;
            while ((inputByte = input.read()) != -1) {
                pos++;
                if ((pos == 0) && (inputByte == 0xEF) && StandardCharsets.UTF_8.equals(charset)) {
                    //utf-8 bom security
                } else if ((pos == 1) && (inputByte == 0xBB) && StandardCharsets.UTF_8.equals(charset)) {
                    //utf-8 bom security
                } else if ((pos == 2) && (inputByte == 0xBF) && StandardCharsets.UTF_8.equals(charset)) {
                    //utf-8 bom security
                } else if (inputByte == '"') {
                    //string
                    string = !string;
                    if (string && (lastByte == '"')) value.write('"');
                } else if (inputByte == separator) {
                    //value separator
                    if (string) {
                        value.write(inputByte); //Complète la chaîne texte
                    } else {
                        data.setValue(row, col, toString(value.toByteArray(), charset)); //Enregistre la valeur actuelle
                        value.reset();
                        data.setValue(row, ++col, ""); //Déclare la valeur suivante
                    }
                } else if ((inputByte == '\r') || (inputByte == '\n')) {
                    //new line
                    if (string) {
                        //new line in string
                        value.write(inputByte);
                    } else if ((lastByte != '\r') && (lastByte != '\n')) {
                        //new line as row
                        data.setValue(row, col, toString(value.toByteArray(), charset));
                        value.reset();
                        row++;
                        col = 0;
                    }
                } else {
                    //value
                    value.write(inputByte);
                }
                lastByte = inputByte;
            }
            if (value.size() > 0)
                data.setValue(row, col, toString(value.toByteArray(), charset));
            return data;
        };
    }

}
