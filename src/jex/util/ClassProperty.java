package jex.util;

import java.util.Objects;
import java.util.function.BiConsumer;
import java.util.function.Function;

/**
 * Représentation d'une propriété de classe.
 *
 * @param <T> Type de classe
 */
public class ClassProperty<T> {

    private final String name;
    private final Function<T, String> getter;
    private final BiConsumer<T, String> setter;

    public ClassProperty(String name, Function<T, String> getter) {
        this(name, Objects.requireNonNull(getter), null);
    }

    public ClassProperty(String name, Function<T, String> getter, BiConsumer<T, String> setter) {
        this.name = name;
        this.getter = Objects.requireNonNull(getter);
        this.setter = setter;
    }

    public String getName() {
        return name;
    }

    public String getValue(T object) {
        if (getter == null) throw new UnsupportedOperationException();
        return getter.apply(object);
    }

    public <V> V getValue(T object, Function<String, V> parser) {
        Objects.requireNonNull(parser);
        return parser.apply(getValue(object));
    }

    public void setValue(T object, String value) {
        if (setter == null) throw new UnsupportedOperationException();
        setter.accept(object, value);
    }

    public <V> void setValue(T object, V value, Function<V, String> composer) {
        Objects.requireNonNull(composer);
        setValue(object, composer.apply(value));
    }

    public boolean isReadOnly() {
        return (setter != null);
    }

}
