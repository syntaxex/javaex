package jex.util;

import java.lang.ref.WeakReference;

public class GridRowKey<V> {

    private final WeakReference<GridTable<V>> tableReference;

    GridRowKey(GridTable<V> table) {
        this.tableReference = new WeakReference<>(table);
    }

    @Override
    public String toString() {
        GridTable<V> table = tableReference.get();
        return String.valueOf((table != null) ? table.getRows().indexOf(this) + 1 : 0);
    }

}
