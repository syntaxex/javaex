package jex.util;

import java.io.IOException;
import java.io.OutputStream;

/**
 * Interface pour les composants d'export des KeyTable.
 */
public interface KeyTableExporter<R, C, V> {

    /**
     * Exporte la KeyTable.
     *
     * @param keyTable KeyTable
     * @param output   Flux de sortie
     * @throws IOException Exception levée lors de l'écriture
     */
    void output(KeyTable<R, C, V> keyTable, OutputStream output) throws IOException;

}
