package jex.util;

import java.io.IOException;
import java.io.InputStream;

/**
 * Interface pour les composants d'import d'une StringGridTable.
 */
public interface StringGridTableImporter extends GridTableImporter<String> {

    /**
     * Importe vers une StringGridTable.
     *
     * @param input Flux d'entrée
     * @throws IOException Exception levée lors de la lecture
     */
    StringGridTable input(InputStream input) throws IOException;

}
