package jex.util;

import java.io.*;
import java.util.List;
import java.util.Objects;

/**
 * Classe pour gérer une table de données sous forme de fichier.
 */
public class KeyTableFile<R, C, V> implements Table<R, C, V> {

    private final File file;
    private final KeyTableImporter<R, C, V> importer;
    private final KeyTableExporter<R, C, V> exporter;
    private KeyTable<R, C, V> table;

    public KeyTableFile(File file, KeyTableImporter<R, C, V> importer, KeyTableExporter<R, C, V> exporter) throws IOException {
        this.file = file;
        this.importer = Objects.requireNonNull(importer);
        this.exporter = Objects.requireNonNull(exporter);
        this.table = new KeyTable<>();
        load();
    }

    private synchronized void load() throws IOException {
        this.table = importer.input(new FileInputStream(file));
    }

    private synchronized void save() throws IOException {
        table.export(exporter, new FileOutputStream(file));
    }

    @Override
    public synchronized boolean createRow(R key) throws IOException {
        load();
        boolean result = table.createRow(key);
        save();
        return result;
    }

    @Override
    public synchronized boolean existRow(R key) throws IOException {
        load();
        boolean result = table.existRow(key);
        save();
        return result;
    }

    @Override
    public synchronized boolean existRowValues(R key) throws IOException {
        load();
        boolean result = table.existRowValues(key);
        save();
        return result;
    }

    @Override
    public synchronized void clearRow(R key) throws IOException {
        load();
        table.clearRow(key);
        save();
    }

    @Override
    public synchronized void deleteRow(R key) throws IOException {
        load();
        table.deleteRow(key);
        save();
    }

    @Override
    public synchronized List<R> getRows() throws IOException {
        load();
        return table.getRows();
    }

    @Override
    public synchronized int countRows() throws IOException {
        load();
        return table.countRows();
    }

    @Override
    public synchronized List<V> getRowValues(R row, V defaultValue) throws IOException {
        load();
        return table.getRowValues(row, defaultValue);
    }

    @Override
    public synchronized boolean createColumn(C key) throws IOException {
        load();
        boolean result = table.createColumn(key);
        save();
        return result;
    }

    @Override
    public synchronized boolean existColumn(C key) throws IOException {
        load();
        return table.existColumn(key);
    }

    @Override
    public synchronized boolean existColumnValues(C key) throws IOException {
        load();
        return table.existColumnValues(key);
    }

    @Override
    public synchronized void clearColumn(C key) throws IOException {
        load();
        table.clearColumn(key);
        save();
    }

    @Override
    public synchronized void deleteColumn(C key) throws IOException {
        load();
        table.deleteColumn(key);
        save();
    }

    @Override
    public synchronized List<C> getColumns() throws IOException {
        load();
        return table.getColumns();
    }

    @Override
    public synchronized int countColumns() throws IOException {
        load();
        return table.countColumns();
    }

    @Override
    public synchronized List<V> getColumnValues(C col, V defaultValue) throws IOException {
        load();
        return table.getColumnValues(col, defaultValue);
    }

    @Override
    public synchronized V getValue(R rowKey, C colKey, V defaultValue) throws IOException {
        load();
        return table.getValue(rowKey, colKey, defaultValue);
    }

    @Override
    public synchronized V requestValue(R rowKey, C colKey, V defaultValue) throws IOException, KeyNotFoundException {
        load();
        return table.requestValue(rowKey, colKey, defaultValue);
    }

    @Override
    public synchronized void setValue(R rowKey, C colKey, V value) throws IOException {
        load();
        table.setValue(rowKey, colKey, value);
        save();
    }

    @Override
    public synchronized void updateValue(R rowKey, C colKey, V value) throws IOException, KeyNotFoundException {
        load();
        table.updateValue(rowKey, colKey, value);
        save();
    }

    @Override
    public synchronized boolean existValue(R rowKey, C colKey) throws IOException {
        load();
        return table.existValue(rowKey, colKey);
    }

    @Override
    public synchronized void deleteValue(R rowKey, C colKey) throws IOException {
        load();
        table.deleteValue(rowKey, colKey);
        save();
    }

    @Override
    public synchronized void deleteAll() throws IOException {
        load();
        table.deleteAll();
        save();
    }

    @Override
    public <N, M, W> Table<N, M, W> convert(Converter<R, N> rowKeyConverter, Converter<C, M> columnKeyConverter, Converter<V, W> valueConverter) {
        return table.convert(rowKeyConverter, columnKeyConverter, valueConverter);
    }

    @Override
    public synchronized void export(KeyTableExporter<R, C, V> exporter, OutputStream output) throws IOException {
        table.export(exporter, output);
    }

}
