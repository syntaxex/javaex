package jex.util;

import java.io.IOException;
import java.util.List;

/**
 * Classe pour gérer une table de type grille.
 */
public class GridTable<V> extends KeyTable<GridRowKey<V>, GridColumnKey<V>, V> {

    /**
     * Retourne la ligne correspondant à l'index.
     *
     * @param index Index de ligne
     * @return Ligne
     */
    public synchronized GridRowKey<V> getRowKey(int index) {
        if (index < 0) throw new IndexOutOfBoundsException();
        if (index >= countRows()) throw new IndexOutOfBoundsException();
        return super.getRows().get(index);
    }

    /**
     * Augmente le nombre de lignes.
     *
     * @param index Indice max (base 0)
     */
    private void extendRows(int index) {
        if (index < 0) throw new IndexOutOfBoundsException();
        while (countRows() <= index) {
            createRow(new GridRowKey<>(this));
        }
    }

    /**
     * Retourne les valeurs d'une ligne.
     *
     * @param index        Index de ligne
     * @param defaultValue Valeur par défaut
     * @return Valeurs
     */
    public synchronized List<V> getRowValues(int index, V defaultValue) {
        return super.getRowValues(getRowKey(index), defaultValue);
    }

    /**
     * Retourne la colonne correspondant à l'index.
     *
     * @param index Index de colonne
     * @return Colonne
     */
    public synchronized GridColumnKey<V> getColumnKey(int index) {
        if (index < 0) throw new IndexOutOfBoundsException();
        if (index >= countColumns()) throw new IndexOutOfBoundsException();
        return super.getColumns().get(index);
    }

    /**
     * Augmente le nombre de colonnes.
     *
     * @param index Indice max (base 0)
     */
    private synchronized void extendColumns(int index) {
        if (index < 0) throw new IndexOutOfBoundsException();
        while (countColumns() <= index) {
            createColumn(new GridColumnKey<>(this));
        }
    }

    /**
     * Retourne les valeurs d'une colonne.
     *
     * @param index        Index de colonne
     * @param defaultValue Valeur par défaut
     * @return Valeurs
     */
    public synchronized List<V> getColumnValues(int index, V defaultValue) {
        return super.getColumnValues(getColumnKey(index), defaultValue);
    }

    /**
     * Retourne une valeur.
     *
     * @param rowIndex     Index de la ligne
     * @param colIndex     Index de la colonne
     * @param defaultValue Valeur par défaut si la ligne ou la colonne n'est pas trouvée, ou si la valeur est vide
     * @return Valeur
     */
    public synchronized V getValue(int rowIndex, int colIndex, V defaultValue) {
        return super.getValue(getRowKey(rowIndex), getColumnKey(colIndex), defaultValue);
    }

    /**
     * Ajoute ou modifie la valeur. Les lignes et colonnes non présentes sont créées.
     *
     * @param rowIndex Index de la ligne
     * @param colIndex Index de la colonne
     * @param value    Valeur
     * @throws IOException Erreur d'accès aux données
     */
    public synchronized void setValue(int rowIndex, int colIndex, V value) {
        extendRows(rowIndex);
        extendColumns(colIndex);
        super.setValue(getRowKey(rowIndex), getColumnKey(colIndex), value);
    }

    /**
     * Redimensionne la grille. Les valeurs en dehors de la nouvelle taille sont supprimées.
     *
     * @param nbRows Nombre de lignes (supérieur ou égal à 0)
     * @param nbCols Nombre de colonnes (supérieur ou égal à 0)
     */
    public synchronized void resize(int nbRows, int nbCols) {
        if (nbRows < 0) throw new IllegalArgumentException();
        if (nbCols < 0) throw new IllegalArgumentException();
        if (nbRows < countRows()) extendRows(nbRows - 1);
        if (nbRows > countRows()) while (countRows() > nbRows) deleteRow(getRows().getLast());
        if (nbCols < countColumns()) extendColumns(nbCols - 1);
        if (nbCols > countColumns()) while (countColumns() > nbCols) deleteColumn(getColumns().getLast());
    }

}
