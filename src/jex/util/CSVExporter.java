package jex.util;

import java.io.OutputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Objects;

public class CSVExporter {

    public static final Converter<Object, String> DEFAULT_CONVERTOR = val -> (val != null) ? val.toString() : "";

    private Charset charset = StandardCharsets.UTF_8;
    private char separator = ',';

    public CSVExporter() {
    }

    public CSVExporter(Charset charset) {
        this.charset = Objects.requireNonNull(charset);
    }

    public CSVExporter(char separator) {
        this.separator = separator;
    }

    public CSVExporter(Charset charset, char separator) {
        this.charset = Objects.requireNonNull(charset);
        this.separator = separator;
    }

    public Charset getCharset() {
        return charset;
    }

    public synchronized void setCharset(Charset charset) {
        this.charset = (charset != null) ? charset : StandardCharsets.UTF_8;
    }

    public char getSeparator() {
        return separator;
    }

    public void setSeparator(char separator) {
        this.separator = separator;
    }

    /**
     * Retourne un exporteur de KeyTable.
     *
     * @param tableKey Clé de la table à écrire dans la première cellule lorsque 'exportRowKey' et 'exportColumnKey' sont demandés
     * @return Exporteur
     */
    public <R, C, V> KeyTableExporter<R, C, V> getKeyTableExporter(String tableKey) {
        return getKeyTableExporter(tableKey, DEFAULT_CONVERTOR, DEFAULT_CONVERTOR, DEFAULT_CONVERTOR, null);
    }

    /**
     * Retourne un exporteur de KeyTable.
     *
     * @param tableKey        Clé de la table à écrire dans la première cellule au départ des clés (ignoré si un convertisseur est manquant)
     * @param rowKeyConvertor Convertisseur de clés de ligne (ou NULL pour ignorer les clés)
     * @param colKeyConvertor Convertisseur de clés de colonne (ou NULL pour ignorer les clés)
     * @param valueConvertor  Convertisseur de valeurs (ou NULL pour le convertisseur par défaut)
     * @param defaultValue    Valeur par défaut pour les valeurs manquantes
     * @return Exporteur
     */
    public <R, C, V> KeyTableExporter<R, C, V> getKeyTableExporter(
            String tableKey,
            Converter<? super R, String> rowKeyConvertor,
            Converter<? super C, String> colKeyConvertor,
            Converter<? super V, String> valueConvertor,
            V defaultValue) {
        return (KeyTable<R, C, V> keyTable, OutputStream output) -> {
            byte[] outputSeparator = String.valueOf(separator).getBytes(charset);
            List<R> rows = keyTable.getRows();
            List<C> cols = keyTable.getColumns();
            boolean exportRowKey = (rowKeyConvertor != null);
            boolean exportColumnKey = (colKeyConvertor != null);
            Converter<? super V, String> validValueConvertor = (valueConvertor != null)
                    ? valueConvertor
                    : DEFAULT_CONVERTOR;
            boolean firstColumn;
            //HEAD
            if (exportColumnKey) {
                //name
                if (exportRowKey) {
                    output.write(CSVUtils.toCSVValue(tableKey, separator).getBytes(charset));
                    firstColumn = false;
                } else {
                    firstColumn = true;
                }
                //colKeys
                for (C colKey : cols) {
                    if (!firstColumn) output.write(outputSeparator);
                    output.write(CSVUtils.toCSVValue(colKeyConvertor.convert(colKey), separator).getBytes(charset));
                    firstColumn = false;
                }
                //eol
                output.write("\r\n".getBytes(charset));
            }
            //ROWS
            for (R rowKey : rows) {
                //rowKey
                if (exportRowKey) {
                    output.write(CSVUtils.toCSVValue(rowKeyConvertor.convert(rowKey), separator).getBytes(charset));
                    firstColumn = false;
                } else {
                    firstColumn = true;
                }
                //values
                for (C colKey : cols) {
                    if (!firstColumn) output.write(outputSeparator);
                    output.write(CSVUtils.toCSVValue(validValueConvertor.convert(keyTable.getValue(rowKey, colKey, defaultValue)), separator).getBytes(charset));
                    firstColumn = false;
                }
                //eol
                output.write("\r\n".getBytes(charset));
            }
            //EOF
            output.flush();
        };
    }

    /**
     * Retourne un exporteur d'une GridTable ou pour uniquement le contenu d'une KeyTable.
     *
     * @return Exporteur
     */
    public <R, C, V> KeyTableExporter<R, C, V> getGridTableExporter() {
        return getKeyTableExporter(null, null, null, DEFAULT_CONVERTOR, null);
    }

}
