package jex.util;

/**
 * Classe de fonctionnalités liées au Système.
 */
public class SystemUtils {

    private SystemUtils() {
    }

    /**
     * Retourne le dossier de travail en cours.
     *
     * @return Dossier de travail actuel
     */
    public static String getWorkingDirectory() {
        return System.getProperty("user.dir");
    }

    /**
     * Retourne le nom de l'utilisateur de la session.
     *
     * @return Nom de l'utilisateur
     */
    public static String getUserName() {
        return System.getProperty("user.name");
    }

    /**
     * Retourne le nom du dossier utilisateur de la session.
     *
     * @return Nom du dossier utilisateur
     */
    public static String getUserFolderName() {
        String[] path = System.getProperty("user.home").split("[\\\\/]");
        String name = (path.length > 0) ? path[path.length - 1] : "";
        return name;
    }

    /**
     * Retourne le chemin local de la session utilisateur.
     *
     * @return Chemin de l'utilisateur
     */
    public static String getUserPath() {
        return System.getProperty("user.home");
    }

}
