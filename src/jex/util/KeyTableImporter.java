package jex.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Interface pour les composants d'import d'une KeyTable.
 */
public interface KeyTableImporter<R, C, V> {

    /**
     * Importe vers une KeyTable.
     *
     * @param input   Flux d'entrée
     * @throws IOException Exception levée lors de la lecture
     */
    KeyTable<R, C, V> input(InputStream input) throws IOException;

}
