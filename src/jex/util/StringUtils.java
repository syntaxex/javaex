package jex.util;

import java.nio.ByteBuffer;
import java.nio.charset.CharacterCodingException;
import java.nio.charset.StandardCharsets;
import java.text.Normalizer;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Classe utilitaire pour la gestion des chaînes.
 */
public class StringUtils {

    /**
     * Test si une chaîne est NULL ou vide.
     *
     * @param value Valeur à tester
     * @return Résultat
     */
    public static boolean isNullOrEmpty(String value) {
        return (value == null) || (value.isBlank());
    }

    /**
     * Test si une chaîne est NULL ou sans caractères visibles.
     *
     * @param value Valeur à tester
     * @return Résultat
     */
    public static boolean isNullOrBlank(String value) {
        return (value == null) || (value.isBlank());
    }

    /**
     * Remplace une chaîne NULL par une chaîne de substitution.
     *
     * @param value     Valeur à tester
     * @param nullValue Valeur de substitution
     * @return Valeur d'origine si différente de NULL, sinon valeur de substitution
     */
    public static String replaceIfNull(String value, String nullValue) {
        return (value != null) ? value : nullValue;
    }

    /**
     * Combine une suite de 'String'.
     *
     * @param sep  Séparateur
     * @param vals Valeurs
     * @return Chaîne finale
     */
    public static String join(char sep, String... vals) {
        return join(String.valueOf(sep), false, Arrays.asList(vals));
    }

    /**
     * Combine une suite de 'String'.
     *
     * @param sep  Séparateur
     * @param vals Valeurs
     * @return Chaîne finale
     */
    public static String join(CharSequence sep, String... vals) {
        return join(sep, false, Arrays.asList(vals));
    }

    /**
     * Combine une suite de 'String'.
     *
     * @param sep  Séparateur
     * @param vals Valeurs
     * @return Chaîne finale
     */
    public static String join(char sep, Collection<String> vals) {
        return join(String.valueOf(sep), false, vals);
    }

    /**
     * Combine une suite de 'String'.
     *
     * @param sep  Séparateur
     * @param vals Valeurs
     * @return Chaîne finale
     */
    public static String join(CharSequence sep, Collection<String> vals) {
        return join(sep, false, vals);
    }

    /**
     * Combine une suite de 'String'.
     *
     * @param sep         Séparateur
     * @param ignoreEmpty Ignorer les valeurs vides
     * @param vals        Valeurs
     * @return Chaîne finale
     */
    public static String join(CharSequence sep, boolean ignoreEmpty, String... vals) {
        return join(sep, ignoreEmpty, Arrays.asList(vals));
    }

    /**
     * Combine une suite de 'String'.
     *
     * @param sep         Séparateur
     * @param ignoreEmpty Ignorer les valeurs vides
     * @param vals        Valeurs
     * @return Chaîne finale
     */
    public static String join(CharSequence sep, boolean ignoreEmpty, Collection<String> vals) {
        StringBuilder value = new StringBuilder();
        int index = 0;
        for (String el : vals) {
            String val = (el == null) ? "" : el;
            if (ignoreEmpty && val.isEmpty()) continue;
            if (index > 0) value.append(sep);
            value.append(val);
            index++;
        }
        return value.toString();
    }

    /**
     * Retire les accents d'une chaîne.
     *
     * @param value Chaine à traiter
     * @return Chaîne sans accents
     */
    public static String unaccent(String value) {
        String normalizedValue = Normalizer.normalize(value, Normalizer.Form.NFD);
        normalizedValue = normalizedValue.replaceAll("[^\\p{ASCII}]", "");
        return normalizedValue;
    }

    /**
     * Sépare les mots d'une chaîne texte.
     *
     * @param value Chaîne texte
     * @param trim  Trimer les éléments
     * @return Liste des mots
     */
    public static List<String> splitWords(String value, boolean trim) {
        if (value == null) return Collections.emptyList();
        return Arrays.stream(value.split("\\s"))
                .map(word -> trim ? word.trim() : word)
                .toList();
    }

    /**
     * Extrait les ensembles numériques des ensembles texte d'une chaîne texte.
     *
     * @param value Texte à découper
     * @param trim  Trimmer les éléments
     * @return Liste des ensembles
     */
    public static List<String> splitNumerics(String value, boolean trim) {
        if (value == null) return Collections.emptyList();
        Pattern pattern = Pattern.compile("(\\d+)|(\\D+)"); // "\\d+" >>> un ou plusieurs chiffres et "\\D+" >>> un ou plusieurs caractères non-chiffres
        List<String> elements = new ArrayList<>();
        Matcher matcher = pattern.matcher(trim ? value.trim() : value);
        while (matcher.find()) elements.add(trim ? matcher.group().trim() : matcher.group());
        return Collections.unmodifiableList(elements);
    }

    /**
     * Normalise une chaîne texte pour une recherche en supprimant les accents et la casse.
     *
     * @param value Chaîne à traiter
     * @return Chaîne normalisée
     */
    public static String normalizeStringToSearch(String value) {
        return unaccent(value.toLowerCase());
    }

    /**
     * Construit un assemblage FullText sans accent, casse et séparateur.
     *
     * @param value Chaîne à traiter
     * @return Chaîne FullText
     */
    public static String buildFullTextString(String value) {
        return StringUtils.normalizeStringToSearch(value
                .replace("\r", "")
                .replace("\n", "")
                .replace("\t", "")
                .replace(".", "")
                .replace("/", "")
                .replace("-", "")
                .replace(" ", "")
                .toLowerCase()
                .trim());
    }

    /**
     * Prepare une chaîne pour une comparaison de son contenu.
     *
     * @param value         Valeur à traiter
     * @param trim          Le contenu doit être trimé
     * @param ignoreCase    La casse doit être ignoré
     * @param ignoreAccents Les accents doivent être ignorés
     * @return Chaîne préparée pour la comparaison
     */
    private static String prepareToCompare(String value, boolean trim, boolean ignoreCase, boolean ignoreAccents) {
        String norm = (value != null) ? value : "";
        if (trim) norm = norm.trim();
        if (ignoreCase) norm = norm.toLowerCase();
        if (ignoreAccents) norm = unaccent(norm);
        return norm;
    }

    /**
     * Test si deux chaînes sont identiques.
     *
     * @param str1       Chaine 1
     * @param str2       Chaine 2
     * @param trim       Ignore les caractères spéciaux qui encadrent les chaines de caractères
     * @param ignoreCase Ignore la casse
     * @return {@code true} Si les chaînes sont identiques
     */
    public static boolean isEquals(String str1, String str2, boolean trim, boolean ignoreCase) {
        return isEquals(str1, str2, trim, ignoreCase, false);
    }

    /**
     * Test si deux chaînes sont identiques.
     *
     * @param str1          Chaine 1
     * @param str2          Chaine 2
     * @param trim          Ignore les caractères spéciaux qui encadrent les chaines de caractères
     * @param ignoreCase    Ignore la casse
     * @param ignoreAccents Ignore les accents
     * @return {@code true} Si les chaînes sont identiques
     */
    public static boolean isEquals(String str1, String str2, boolean trim, boolean ignoreCase, boolean ignoreAccents) {
        String val1 = prepareToCompare(str1, trim, ignoreCase, ignoreAccents);
        String val2 = prepareToCompare(str2, trim, ignoreCase, ignoreAccents);
        return Objects.equals(val1, val2);
    }

    /**
     * Recherche une série de mots dans une chaine.
     *
     * @param value      Chaine
     * @param prefix     Prefix
     * @param trim       Supprime les caractères spéciaux qui encadrent les chaines de caractères
     * @param ignoreCase Ignore la casse
     * @return {@code true} Si les mots sont présents ou si la recherche est vide
     */
    public static boolean startWith(String value, String prefix, boolean trim, boolean ignoreCase) {
        String val1 = prepareToCompare(value, trim, ignoreCase, false);
        String val2 = prepareToCompare(prefix, trim, ignoreCase, false);
        return val1.startsWith(val2);
    }

    /**
     * Recherche une série de mots dans une chaine.
     *
     * @param data   Chaîne à analyser
     * @param search Série de mots
     * @return {@code true} Si les mots sont présents ou si la recherche est vide
     */
    public static boolean searchWordsInString(String data, String search) {
        if (search == null) return true;
        if (data == null) data = "";
        String[] words = normalizeStringToSearch(search).split("[- \\t\\n\\x0B\\f\\r]");
        String source = normalizeStringToSearch(data);
        for (String word : words) {
            if (word == null) continue;
            if (word.isBlank()) continue;
            if (!source.contains(word)) return false;
        }
        return true;
    }

    /**
     * Test si un contenu est de type UTF-8.
     *
     * @param content Contenu
     * @return Vrai si UTF-8 trouvé
     */
    public static boolean isUTF8(byte[] content) {
        try {
            StandardCharsets.UTF_8.newDecoder().decode(ByteBuffer.wrap(content));
        } catch (CharacterCodingException e) {
            return false;
        }
        return true;
    }

    /**
     * Supprime les retours à la ligne d'une chaîne texte.
     *
     * @param value     Chaîne texte à traiter
     * @param separator Séparateur de substitution
     * @return Chaîne texte traitée
     */
    public static String removeLines(String value, CharSequence separator) {
        if (value == null) return null;
        return String.join((separator != null) ? separator : "", value.lines().toList());
    }

}
