package jex.util;

/**
 * Interface de normalisation de valeur.
 *
 * @param <T> Type de valeur
 */
public interface Normalizer<T> {
    /**
     * Normalise une valeur.
     *
     * @param val Valeur
     * @return Valeur normalisée
     */
    T normalize(T val);
}
