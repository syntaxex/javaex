package jex.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Objects;

/**
 * Classe pour gérer une base de données sous formes de tables.
 */
public class KeyTable<R, C, V> implements Table<R, C, V> {

    private final LinkedHashSet<R> rows;
    private final LinkedHashSet<C> cols;
    private final HashMap<DoubleKey<R, C>, V> values;

    public KeyTable() {
        this.cols = new LinkedHashSet<>();
        this.rows = new LinkedHashSet<>();
        this.values = new HashMap<>();
    }

    /**
     * Importe des données dans une KeyTable.
     *
     * @param importer Outil d'import des données
     * @param input    Flux d'entrée
     * @throws IOException Exception levée lors de l'écriture
     */
    public static <R, C, V> KeyTable<R, C, V> from(KeyTableImporter<R, C, V> importer, InputStream input) throws IOException {
        return Objects.requireNonNull(importer).input(Objects.requireNonNull(input));
    }

    @Override
    public synchronized boolean createRow(R key) {
        return rows.add(key);
    }

    @Override
    public synchronized boolean existRow(R key) {
        return rows.contains(key);
    }

    @Override
    public synchronized boolean existRowValues(R key) {
        return values.entrySet().stream().anyMatch(entry -> Objects.equals(entry.getKey().getKey1(), key));
    }

    @Override
    public synchronized void clearRow(R key) {
        cols.forEach(col -> deleteValue(key, col));
    }

    @Override
    public synchronized void deleteRow(R key) {
        clearRow(key);
        rows.remove(key);
    }

    @Override
    public synchronized List<R> getRows() {
        return List.copyOf(rows);
    }

    @Override
    public synchronized int countRows() {
        return rows.size();
    }

    @Override
    public synchronized List<V> getRowValues(R row, V defaultValue) {
        return cols.stream().map(col -> getValue(row, col, defaultValue)).toList();
    }

    @Override
    public synchronized boolean createColumn(C key) {
        return cols.add(key);
    }

    @Override
    public synchronized boolean existColumn(C key) {
        return cols.contains(key);
    }

    @Override
    public synchronized boolean existColumnValues(C key) {
        return values.entrySet().stream().anyMatch(entry -> Objects.equals(entry.getKey().getKey2(), key));
    }

    @Override
    public synchronized void clearColumn(C key) {
        rows.forEach(row -> deleteValue(row, key));
    }

    @Override
    public synchronized void deleteColumn(C key) {
        clearColumn(key);
        cols.remove(key);
    }

    @Override
    public synchronized List<C> getColumns() {
        return List.copyOf(cols);
    }

    @Override
    public synchronized int countColumns() {
        return cols.size();
    }

    @Override
    public synchronized List<V> getColumnValues(C col, V defaultValue) {
        return rows.stream().map(row -> getValue(row, col, defaultValue)).toList();
    }

    @Override
    public synchronized V getValue(R rowKey, C colKey, V defaultValue) {
        return values.getOrDefault(new DoubleKey<>(rowKey, colKey), defaultValue);
    }

    @Override
    public synchronized V requestValue(R rowKey, C colKey, V defaultValue) throws KeyNotFoundException {
        if (!existRow(rowKey)) throw new KeyNotFoundException();
        if (!existColumn(colKey)) throw new KeyNotFoundException();
        return values.getOrDefault(new DoubleKey<>(rowKey, colKey), defaultValue);
    }

    @Override
    public void setValue(R rowKey, C colKey, V value) {
        if (!existRow(rowKey)) createRow(rowKey);
        if (!existColumn(colKey)) createColumn(colKey);
        try {
            updateValue(rowKey, colKey, value);
        } catch (KeyNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public synchronized void updateValue(R rowKey, C colKey, V value) throws KeyNotFoundException {
        if (!existRow(rowKey) || !existColumn(colKey)) throw new KeyNotFoundException();
        values.put(new DoubleKey<>(rowKey, colKey), value);
    }

    @Override
    public synchronized boolean existValue(R rowKey, C colKey) {
        return values.containsKey(new DoubleKey<>(rowKey, colKey));
    }

    @Override
    public synchronized void deleteValue(R rowKey, C colKey) {
        values.remove(new DoubleKey<>(rowKey, colKey));
    }

    @Override
    public synchronized void deleteAll() {
        values.clear();
        rows.clear();
        cols.clear();
    }

    @Override
    public <N, M, W> Table<N, M, W> convert(Converter<R, N> rowKeyConverter, Converter<C, M> columnKeyConverter, Converter<V, W> valueConverter) {
        Objects.requireNonNull(rowKeyConverter);
        Objects.requireNonNull(columnKeyConverter);
        Objects.requireNonNull(valueConverter);
        KeyTable<N, M, W> newTable = new KeyTable<>();
        HashMap<R, N> newRowKeys = new HashMap<>();
        HashMap<C, M> newColKeys = new HashMap<>();
        getRows().forEach(r -> newRowKeys.put(r, rowKeyConverter.convert(r)));
        getColumns().forEach(c -> newColKeys.put(c, columnKeyConverter.convert(c)));
        newTable.rows.addAll(newRowKeys.values());
        newTable.cols.addAll(newColKeys.values());
        values.forEach((ref, v) -> newTable.values.put(
                new DoubleKey<>(newRowKeys.get(ref.getKey1()), newColKeys.get(ref.getKey2())),
                valueConverter.convert(v)));
        return newTable;
    }

    @Override
    public synchronized void export(KeyTableExporter<R, C, V> exporter, OutputStream output) throws IOException {
        Objects.requireNonNull(exporter).output(this, Objects.requireNonNull(output));
    }

}
