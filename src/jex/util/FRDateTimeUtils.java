package jex.util;

import java.time.format.DateTimeFormatter;

/**
 * Classe utilitaire de gestion des dates avec la prise en charge des dates françaises.
 */
public final class FRDateTimeUtils {

    /**
     * Format français de date-heure avec les secondes : "dd/MM/yyyy HH:mm:ss"
     */
    public static final DateTimeFormatter FR_LOCAL_DATETIME = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");
    /**
     * Format français de date-heure sans les secondes : "dd/MM/yyyy HH:mm"
     */
    public static final DateTimeFormatter FR_LOCAL_DATETIME_WITHOUTSECONDS = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm");
    /**
     * Date au format français "dd/MM/yyyy"
     */
    public static final DateTimeFormatter FR_LOCAL_DATE = DateTimeFormatter.ofPattern("dd/MM/yyyy");

    private FRDateTimeUtils() {
    }

}
